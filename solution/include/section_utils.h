//
// Created by vyacheslav on 4/1/23.
//

#ifndef SECTION_UTILS_H
#define SECTION_UTILS_H

#include "pe_file.h"
#include "stdint.h"
#include <stdio.h>

/// @brief Structure with raw section data and its size
struct Section {
    /// data size
    uint32_t size;
    /// raw data
    uint8_t* data;
};

struct SectionHeader* find_section(char* name, struct PEFile* peFile);
int read_section(FILE* file, struct SectionHeader* header, struct Section* section);

#endif //SECTION_UTILS_H
