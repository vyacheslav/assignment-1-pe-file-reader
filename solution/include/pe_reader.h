//
// Created by vyacheslav on 4/1/23.
//

#ifndef PE_READER_H
#define PE_READER_H

#include "pe_file.h"
#include <stdio.h>

int read_pe(FILE* file, struct PEFile* pe);

#endif //PE_READER_H
