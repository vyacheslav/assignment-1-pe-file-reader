/// @file 
/// @brief Main application file

#include "pe_file.h"
#include "pe_reader.h"
#include "section_utils.h"
#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Structure to group console arguments
struct Params {
    /// path to the source file
    char* pe_file;
    /// name of desired section
    char* section_name;
    /// path to the output file
    char* out_bimage;
};

/// \brief Print error message and exit with special code
/// \param message Error message
void error(const char* message) {
    printf("%s", message);
    exit(1);
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    if (argc != 4) {
        fprintf(stdout, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
        return 0;
    }
    struct Params params = (struct Params){argv[1], argv[2], argv[3]};

    if (!params.pe_file) error("Invalid pe file error");
    FILE* in = fopen(params.pe_file, "rb");
    if (!in) error("Opening pe file error");
    struct PEFile pe = { 0 };

    if (read_pe(in, &pe)) error("Reading pe file error");
    struct SectionHeader* found_header = find_section(params.section_name, &pe);
    if (found_header == NULL) error("Invalid section name error");

    struct Section section = { 0 };
    if (read_section(in, found_header, &section)) error("Reading section error");

    if (!params.out_bimage) error("Invalid output file error");
    FILE* out = fopen(params.out_bimage, "wb");
    if (!out) error("Opening output file error");
    if (!fwrite(section.data, section.size, 1, out)) error("Writing in output file error");

    fclose(in);
    fclose(out);
    free(pe.header);
    free(pe.section_headers);
    free(section.data);

    return 0;
}
