/// @file
//
// Created by vyacheslav on 4/1/23.
//

#include "section_utils.h"
#include <malloc.h>
#include <stdio.h>
#include <string.h>

/// \brief Looking up section with special name
/// \param name Name to find
/// \param pe Structure with section headers
/// \return Address of the SectionHeader or NULL if not found
struct SectionHeader* find_section(char* name, struct PEFile* pe) {
    for (uint32_t i = 0; i < pe->header->number_of_sections; i++) {
        if (strncmp((char*)pe->section_headers[i].name, name, 8) == 0)
            return pe->section_headers + i;
    }
    return NULL;
}

/// \brief Read section data
/// \param file Filestream to read
/// \param header Header of section
/// \param section Structure to store section data
/// \return Status code
int read_section(FILE* file, struct SectionHeader* header, struct Section* section) {
    section->size = header->rawdata_size;
    section->data = malloc(header->rawdata_size);
    if (fseek(file, header->pointer_to_rawdata, SEEK_SET)) return 1;
    if (!section->data)  return 1;
    if (!fread(section->data, section->size, 1, file)) return 1;
    return 0;
}
