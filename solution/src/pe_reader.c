/// @file
//
// Created by vyacheslav on 4/1/23.
//

#include "pe_reader.h"
#include "stdbool.h"
#include <malloc.h>

/// @brief Main function in module. Malloc memory for headers and read it from filestream into there. All pointers & data save into PEFile.
/// @param file Filestream to read data
/// @param pe Pointer to structure that stores headers and offsets.
int read_pe(FILE* file, struct PEFile* pe) {
    if (fseek(file, OFFSET, SEEK_SET)) return 1;
    if (!fread(&pe->header_offset, 4, 1, file)) return 1;

    pe->header = malloc(sizeof(struct PEHeader));
    if (fseek(file, pe->header_offset, SEEK_SET)) return 1;
    if (!pe->header) return 1;
    if (!fread(pe->header, sizeof(struct PEHeader), 1, file)) return 1;
    
    pe->optional_header_offset = pe->header_offset + sizeof(struct PEHeader);
    pe->section_header_offset = pe->optional_header_offset + pe->header->size_of_optional_header;
    pe->section_headers = malloc(sizeof(struct SectionHeader) * pe->header->number_of_sections);

    if (fseek(file, pe->section_header_offset, SEEK_SET)) return 1;
    if (!pe->section_headers) return 1;
    if (fread(pe->section_headers, sizeof(struct SectionHeader), pe->header->number_of_sections, file) != pe->header->number_of_sections)
        return 1;
    return 0;
}
